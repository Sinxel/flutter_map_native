import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

typedef void MapNativeCreatedCallback(MapNativeController controller);

class MapNative extends StatefulWidget{
  const MapNative({
    Key key,
    this.onMapNativeCreated,
  }):super(key: key);

  final MapNativeCreatedCallback onMapNativeCreated;
  @override
  State<StatefulWidget> createState() => MapNativeState();
}

class MapNativeState extends State<MapNative>{
  @override
  Widget build(BuildContext context){
    return AndroidView(
      viewType: 'com.homyt.mapnative/mapnative',
      onPlatformViewCreated: onMapNativeCreated,
    );

  }

  void onMapReady(){

  }

  void onMapNativeCreated(int id){
    if(widget.onMapNativeCreated==null){
      return;
    }

    widget.onMapNativeCreated(MapNativeController._(id));
  }


}

class MapNativeController{
  MapNativeController._(int id) : _channel = new MethodChannel('com.homyt.mapnative/mapnative_$id');


  final MethodChannel _channel;

  Function onMapReady;
  void listen(){
    _channel.setMethodCallHandler((MethodCall call) async{
      switch(call.method){
        case "callOnMapReady":
          if(onMapReady!=null) onMapReady();
          break;
        default:
          print(call.method);
      }
    });
  }
  Future<void> zoomToFit(LatLng start, LatLng end){
    return _channel.invokeMethod('zoomToFit', <String, dynamic>{

      'start' : start.latitude.toString()+':'+start.longitude.toString(),
      'end' : end.latitude.toString()+':'+end.longitude.toString(),
    });
  }
  Future<void> updateMarker(Marker marker){
    if(marker.id==""){
      throw Exception("Marker was not found");
    }
    return _channel.invokeMethod('updateMarker', <String, dynamic>{
      'id' : marker.id,
      'latitude' : marker.location.latitude,
      'longitude' : marker.location.longitude,
      'title' : marker.title,
      'image' : marker.markerIconPath
    }).then((val){
      if(val!=true)
        throw Exception("Marker was not found");
    });

  }


  Future<void> removePolyline(){
    return _channel.invokeMethod("removePolyline");
  }

  Future<void> addPolyline(Polyline polyline){
    int size = polyline.points.length;
    Map<String, dynamic> data = {"size":size};
    int c = 0;
    polyline.points.forEach((loc){
      data["point_"+c.toString()] = loc.latitude.toString()+":"+loc.longitude.toString();
      c++;
    });
    return _channel.invokeMethod("addPolyline", data);
  }
  Future<void> removeMarker(Marker marker){
    if(marker.id==""){
      print("Marker was not found");
    }
    return _channel.invokeMethod('removeMarker', <String, dynamic>{
      'id' : marker.id
    }).then((val){
      if(val!=true)
        print("Marker was not found");
    });
  }


  Future<void> setBearing(double bearing){
    return _channel.invokeMethod("setBearing", <String, double>{
      'bearing' : bearing
    });
  }

  Future<void> setZoom(double zoom){
    return _channel.invokeMethod("setZoom", <String, double>{
      'zoom' : zoom
    });
  }
  Future<void> addMarker(Marker marker) async{
    assert(marker != null);
    return _channel.invokeMethod('addMarker', <String, dynamic>{
      'latitude' : marker.location.latitude,
      'longitude' : marker.location.longitude,
      'title' : marker.title,
      'image' : marker.markerIconPath
    }).then((val){
      marker.id = val;
    });

  }


  Future<void> setCamera(Camera camera){
    return _channel.invokeMethod("setCamera", <String, dynamic>{
      'latitude' : camera.location.latitude,
      'longitude' : camera.location.longitude,
      'zoom' : camera.zoom,
      'bearing' : camera.bearing,
      'tilt' : camera.tilt
    });
  }

  
}


class Polyline{
  List<LatLng> points;
  Polyline(List<LatLng> points){
    this.points = points;
  }
}

class Marker{
  LatLng location;
  String title;
  String id = "";
  String markerIconPath = "";
  Marker(String title, LatLng location){
    this.title = title;
    this.location = location;
  }
}

class Camera{
  LatLng location;
  double bearing;
  double tilt;
  double zoom;
  Camera(LatLng location, {double bearing = 0.0, double tilt=0.0, double zoom = 17.0}){
    this.location = location;
    this.bearing = bearing;
    this.zoom = zoom;
    this.tilt = tilt;
  }

}

class LatLng{
  double latitude;
  double longitude;
  LatLng(double latitude, double longitude){
    this.latitude = latitude;
    this.longitude = longitude;
  }

  @override
  String toString(){
    return "LatLng {Latitude: $latitude, Longitude: $longitude}";
  }
}