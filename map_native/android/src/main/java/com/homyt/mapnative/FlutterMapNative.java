package com.homyt.mapnative;

import android.content.Context;

import android.graphics.Color;
import android.view.View;


import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.annotations.Polyline;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import java.util.ArrayList;


import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.platform.PlatformView;

import static com.mapbox.mapboxsdk.Mapbox.getApplicationContext;

public class FlutterMapNative implements PlatformView, MethodChannel.MethodCallHandler, OnMapReadyCallback {

    private  MapView mapNative;
    private  MethodChannel methodChannel;
    private  MapboxMap map;
    private ArrayList<FlutterMarker> markers;
    private final Context context;
    private Polyline polyline;
    int markerIndex = 0;
    FlutterMapNative(Context context, BinaryMessenger messenger, int id){
        this.context = context;
        mapNative = new MapView(context);
        mapNative.getMapAsync(this);
        methodChannel = new MethodChannel(messenger, "com.homyt.mapnative/mapnative_"+id);
        methodChannel.setMethodCallHandler(this);
        markers = new ArrayList<>();

    }


    @Override
    public View getView(){
        System.out.println(mapNative);
        return mapNative;
    }

    @Override
    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result){
        if(map==null) return;
        switch(methodCall.method){
            case "addMarker":
                addMarker(methodCall, result);
                break;
            case "updateMarker":
                updateMarker(methodCall, result);
                break;
            case "removeMarker":
                removeMarker(methodCall, result);
                break;
            case "addPolyline":
                addPolyline(methodCall, result);
                break;
            case "removePolyline":
                removePolylines(methodCall, result);
                break;
            case "setBearing":
                setBearing(methodCall, result);
                break;
            case "setZoom":
                setZoom(methodCall, result);
                break;
            case "zoomToFit":
                zoomToFit(methodCall, result);
                break;

            case "setCamera":
                setCamera(methodCall, result);
                break;
            default:
                result.notImplemented();
        }
    }

    private void setBearing(final MethodCall call, MethodChannel.Result result){
        double bearing = ((Number) call.argument("bearing")).doubleValue();
        CameraPosition cp = map.getCameraPosition();
        CameraPosition rotated = new CameraPosition.Builder().bearing(bearing).target(cp.target).tilt(cp.tilt).zoom(cp.zoom).build();
        map.setCameraPosition(rotated);
        result.success(null);

    }

    private void zoomToFit(final MethodCall call, MethodChannel.Result result){


        String start  =  call.argument("start");
        String end  =  call.argument("end");
        String[] locData = start.split(":");
        double lat = Double.parseDouble(locData[0]);
        double lng = Double.parseDouble(locData[1]);
        LatLng sloc = new LatLng(lat, lng);
        CameraPosition cp = new CameraPosition.Builder().
                bearing(0).
                target(sloc).tilt(0).zoom(17).
                build();
        map.setCameraPosition(cp);
        locData = end.split(":");
        lat = Double.parseDouble(locData[0]);
        lng = Double.parseDouble(locData[1]);
        LatLng eloc = new LatLng(lat, lng);
        LatLngBounds target = new LatLngBounds.Builder().include(sloc).include(eloc).build();
        map.animateCamera(CameraUpdateFactory.newLatLngBounds(target, 100));

    }

    private void setZoom(final MethodCall call, MethodChannel.Result result){
        double zoom = ((Number) call.argument("zoom")).doubleValue();
        CameraPosition cp = map.getCameraPosition();
        CameraPosition zoomed = new CameraPosition.Builder().bearing(cp.bearing).target(cp.target).tilt(cp.tilt).zoom(zoom).build();
        map.setCameraPosition(zoomed);

        result.success(null);

    }
    private void setCamera(final MethodCall methodCall, MethodChannel.Result result){
        double lat = ((Number) methodCall.argument("latitude")).doubleValue();
        double lng = ((Number) methodCall.argument("longitude")).doubleValue();
        double zoom = ((Number) methodCall.argument("zoom")).doubleValue();
        double bearing = ((Number) methodCall.argument("bearing")).doubleValue();
        double tilt = ((Number) methodCall.argument("tilt")).doubleValue();
        LatLng location = new LatLng(lat, lng);
        CameraPosition cp = new CameraPosition.Builder()
                .target(location)
                .zoom(zoom)
                .bearing(bearing)
                .tilt(tilt)
                .build();
        map.setCameraPosition(cp);

        result.success(null);
    }

    private void addMarker(MethodCall methodCall, MethodChannel.Result result){

        final MarkerOptions marker = new MarkerOptions();
        double lat = ((Number) methodCall.argument("latitude")).doubleValue();
        double lng = ((Number) methodCall.argument("longitude")).doubleValue();
        String title =  methodCall.argument("title");
        String iconLocation = methodCall.argument("image");

        LatLng location = new LatLng(lat, lng);
        marker.title(title);
        marker.position(location);
        Icon icon;
        if(iconLocation.equals("")){

            icon = IconFactory.getInstance(context).defaultMarker();
        }else{
            icon = IconFactory.getInstance(context).fromPath(iconLocation);
        }
        marker.setIcon(icon);
        Marker ret = map.addMarker(marker);
        markerIndex++;
        String id = "Marker_"+markerIndex;
        FlutterMarker fmarker = new FlutterMarker(id, ret);
        markers.add(fmarker);
        result.success(id);
    }

    private void updateMarker(MethodCall call, MethodChannel.Result result){
        String id = call.argument("id");
        for(int i=0; i<markers.size(); i++){
            if(markers.get(i).getId().equals(id)){
                double lat = ((Number) call.argument("latitude")).doubleValue();
                double lng = ((Number) call.argument("longitude")).doubleValue();
                String title =  call.argument("title");
                String iconLocation = call.argument("image");
                Marker marker = markers.get(i).getMarker();
                marker.setPosition(new LatLng(lat, lng));
                marker.setTitle(title);
                Icon icon;
                if(iconLocation.equals("")){

                    icon = IconFactory.getInstance(context).defaultMarker();
                }else{
                    icon = IconFactory.getInstance(context).fromPath(iconLocation);
                }
                marker.setIcon(icon);
                map.updateMarker(marker);
                result.success(true);
                return;

            }
        }
        result.success(false);
    }

    private void removeMarker(MethodCall call, MethodChannel.Result result){
        String id = call.argument("id");
        for(int i=0; i<markers.size(); i++){
            if(markers.get(i).getId().equals(id)){
                Marker marker = markers.get(i).getMarker();
                map.removeMarker(marker);
                markers.remove(i);
                result.success(true);
                return;

            }
        }
        result.success(false);
    }
    private void addPolyline(MethodCall call, MethodChannel.Result result){
        if(polyline!=null){
            map.removePolyline(polyline);
        }

        PolylineOptions poly = new PolylineOptions();
        int size = ((Number) call.argument("size")).intValue();
        for(int i=0; i<size; i++){
            String argName = "point_" + i;
            String latLng = call.argument(argName);
            String[] locData = latLng.split(":");
            double lat = Double.parseDouble(locData[0]);
            double lng = Double.parseDouble(locData[1]);
            LatLng loc = new LatLng(lat, lng);
            poly.add(loc);
        }
        poly.color(Color.parseColor("#FF0000"));
        poly.width(7.0f);
        polyline  = map.addPolyline(poly);
        result.success(null);
    }

    private void removePolylines(MethodCall call, MethodChannel.Result result){
        if(polyline==null) return;
        map.removePolyline(polyline);
        result.success(null);

    }
    @Override
    public void dispose(){
        map = null;
        mapNative = null;
        markers = null;
        methodChannel = null;
    }

    @Override
    public void onMapReady(MapboxMap mapboxMap) {
        this.map = mapboxMap;
        methodChannel.invokeMethod("callOnMapReady", 10);
    }
}
