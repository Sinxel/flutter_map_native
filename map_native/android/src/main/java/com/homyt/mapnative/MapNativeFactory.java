package com.homyt.mapnative;

import android.content.Context;

import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.StandardMessageCodec;
import io.flutter.plugin.platform.PlatformView;
import io.flutter.plugin.platform.PlatformViewFactory;

public class MapNativeFactory extends PlatformViewFactory {
    private final BinaryMessenger messenger;

    public MapNativeFactory(BinaryMessenger messenger){
        super(StandardMessageCodec.INSTANCE);
        this.messenger = messenger;
    }

    @Override
    public PlatformView create(Context context, int id, Object o){

        return new FlutterMapNative(context, messenger, id);
    }


}
