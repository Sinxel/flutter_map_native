package com.homyt.mapnative;

import com.mapbox.mapboxsdk.annotations.Marker;

public class FlutterMarker {


    private Marker marker;
    private  String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }
    public FlutterMarker(String id, Marker marker){
        this.id = id;
        this.marker = marker;
    }

}
