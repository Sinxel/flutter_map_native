package com.homyt.mapnative;

import io.flutter.plugin.common.PluginRegistry.Registrar;

/** MapNativePlugin */
public class MapNativePlugin{
  /** Plugin registration. */
  public static void registerWith(Registrar registrar) {
   registrar
           .platformViewRegistry()
           .registerViewFactory("com.homyt.mapnative/mapnative", new MapNativeFactory(registrar.messenger()));
  }


}
