import 'dart:async';

import 'package:flutter/material.dart';
import 'package:map_native/map_native.dart';

void main() => runApp(MaterialApp(home: TextViewExample()));

class TextViewExample extends StatelessWidget {
  MapNativeController controller;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('Flutter MapBox Map')),
        body: Stack(

            children: [
          Center(
              child: Container(
                  padding: EdgeInsets.symmetric(vertical: 30.0),
                  child: MapNative(
                    onMapNativeCreated: onMapCreated,
                  ),
              constraints: BoxConstraints.expand(),),),

          Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                  constraints: BoxConstraints(maxHeight: 100),
                  color: Colors.blue[100],
                  child: Center(child: RaisedButton(child: Text("Testing MApbox!"), onPressed: (){
                    controller.setCamera(Camera(LatLng(10.0, 10.0), zoom: 12));
                  }),)))
        ]));
  }

  void onMapCreated(MapNativeController controller) async{
    this.controller = controller;
    controller.onMapReady = onMapReady;
    controller.listen();



  }

  void onMapReady() async{

      Camera camera = new Camera(LatLng(31.9066467, 35.2108430), zoom: 12.0);
      Marker me = Marker("Me", LatLng( 31.901284, 35.210816));
      Marker dest = Marker("Destination", LatLng( 31.9066467, 35.2108430));
      controller.setCamera(camera);
      controller.addMarker(me);
      controller.addMarker(dest);

      Timer timer = Timer(Duration(seconds: 40), (){
        print(dest.id);
        dest.title = "Passenger";
        controller.updateMarker(dest);
      });

      Timer timer2 = Timer(Duration(seconds: 80), (){
        print(dest.id);
        controller.removeMarker(dest);
      });

  }
}