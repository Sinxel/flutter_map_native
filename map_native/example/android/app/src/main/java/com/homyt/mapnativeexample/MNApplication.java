package com.homyt.mapnativeexample;

import android.app.Application;
import android.support.annotation.CallSuper;

import com.mapbox.mapboxsdk.Mapbox;

import io.flutter.view.FlutterMain;

public class MNApplication extends Application {
    @Override
    @CallSuper
    public void onCreate(){
        super.onCreate();
        FlutterMain.startInitialization(this);
        Mapbox.getInstance(getApplicationContext(), "pk.eyJ1IjoicmFlZGZhciIsImEiOiJjamdrczFnaXUxbWZqMzNrMXM3dDMxeDg2In0.4yJiVyuIT-nEHIKYcDjkZg");
    }
}
